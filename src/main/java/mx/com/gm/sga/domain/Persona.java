/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gm.sga.domain;

import java.io.Serializable;

/**
 *
 * @author ELITEBOOK
 */
public class Persona implements Serializable{
   
    private static final long serialVersionID = 1L;
    private int idPersona;
    private String nombre;
    private String aPaterno;
    private String aMaterno;
    private String email;
    private String telefono;
    
    public Persona (){
        
    }
    
    public Persona(int idPersona, String nombre, String aPaterno, String aMaterno, String email, String telefono){
        super();
        this.idPersona = idPersona;
        this.nombre = nombre;
        this.aPaterno = aPaterno;
        this.aMaterno = aMaterno;
        this.email = email;
        this.telefono = telefono;
    }

    public int getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(int idPersona) {
        this.idPersona = idPersona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    @Override
    public String toString(){
    return "Persona ["
            + " idPersona = "+idPersona
            + ", nombre =  "+nombre
            + ", aPaterno = "+aPaterno
            + ", aMaterno = "+aMaterno
            + ", email = "+email
            + ", telefono = "+telefono+""
            + " ]";
    }
}
